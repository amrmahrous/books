<?php
      $this->load->view('header');
?>

         <table id="example" class="display" cellspacing="0" width="100%">
            <thead>
               <tr>
                  <th>Name</th>
                  <th>author</th>
                  <th>copy number</th>
                  <th>status</th>
                  <th>borrow</th>
               </tr>
            </thead>
            <tbody>
            <?php foreach($books AS $book){ ?>
               <tr>
                  <td><?php echo $book->name;?></td>
                  <td><?php echo $book->author;?></td>
                  <td><?php echo $book->copy_nr;?></td>
                  <td><?php echo ($book->status)?'available':'Not available';?></td>
                  <td><?php echo ($book->status)?"<a class='btn-default' href='addborrow/$book->id' >borrow<a>":'' ;?></td>
               </tr>
               <?php } ?>
            </tbody>
         </table>
      </div>
      <script type="text/javascript">
         // For demo to fit into DataTables site builder...
         $('#example')
         	.removeClass( 'display' )
         	.addClass('table table-striped table-bordered');
      </script>
   </body>
</html>