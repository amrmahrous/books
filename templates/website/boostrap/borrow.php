<?php
      $this->load->view('header');
?>

         <table id="example" class="display" cellspacing="0" width="100%">
            <thead>
               <tr>
                  <th>book_name</th>
                  <th>copy number</th>
                  <th>reader_name</th>
                  <th>borrow_date</th>
               </tr>
            </thead>
            <tbody>
            <?php foreach($list AS $row){ ?>
               <tr>
                  <td><?php echo $row->book_name;?></td>
                  <td><?php echo $row->copy_nr;?></td>
                  <td><?php echo $row->reader_name;?></td>
                  <td><?php echo $row->borrow_date;?></td>
               </tr>
               <?php } ?>
            </tbody>
         </table>
      </div>
      <script type="text/javascript">
         // For demo to fit into DataTables site builder...
         $('#example')
         	.removeClass( 'display' )
         	.addClass('table table-striped table-bordered');
      </script>
   </body>
</html>