<?php

class MY_Controller extends CI_Controller {

   
    public function __construct() {

        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->database();
        $this->load->helper('language');
        
        
    }


}

class Admin_Controller extends MY_Controller {

    public function __construct() {

        parent::__construct();  
        $this->load->library('grocery_CRUD');
        $this->load->admin_theme('bootstrap'); 
        $this->current_template_url = base_url("templates/adminpanel/bootstrap");
    }

}

class Website_Controller extends MY_Controller {

   

    public function __construct() {
        parent::__construct();
                
        $current_template = $this->config->item('website_template');
        $this->load->site_theme($current_template);
        $this->current_template_url = base_url("templates/website/".$current_template);
        }
}

