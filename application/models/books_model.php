<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Books_model extends CI_Model
{
	
	public function login()
	{
		$name = $get['name'] = $this->input->post('name');
		$get['password'] = md5($this->input->post('password'));
		$reader = $this->db->get_where('reader',$get)->row();
		if(!empty($reader) AND  $reader->is_admin){
				$this->session->set_userdata('username', $name);
				$this->session->set_userdata('is_admin', 1);
				redirect('admin/books');
			}
		elseif(!empty($reader)){
				$this->session->set_userdata('username', $name);
				redirect('home/listbook');
			}
		return false;
	}
	
	public function listbooks()
	{
		return $this->db->get('books')->result();
	}

	public function addborrow($add = array())
	{
		if(!empty($add)){
			$book_id = $add['book_id'];
			
			$bookdata = $this->db->get_where('books',array('id'=>$book_id))->row();
			$add['copy_nr'] = $bookdata->copy_nr;
			$get = $this->db->get_where('borrows',$add)->result();
			if(empty($get)){
			$add['borrow_date'] = date("Y-m-d H:i:s");
			$insert = $this->db->insert('borrows',$add);
			}
			$data['status'] = "0";
			$this->db->where('id', $book_id);
			$this->db->update('books', $data); 
		}
		return false;
	}

	public function listborrow()
	{
		$this->db->select('b.borrow_date,b.copy_nr,bk.name as book_name,rd.name as reader_name');
		$this->db->from('borrows b');
		$this->db->join('books bk', 'bk.id = b.book_id', 'left');
		$this->db->join('reader rd', 'rd.id = b.reader_id', 'left');
		return $this->db->get()->result();
	}
}
