<?php
class admin extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->head = "Books Library";
       $this->subhead = "list books";
        $this->is_admin = $this->session->userdata('is_admin');
        if(!$this->is_admin)
            redirect('home');
        if($this->username = $this->session->userdata('username'))
       $this->userdata = $this->db->get_where('reader',array('name'=>$this->username))->row();


    }
    function index(){
        redirect('admin/books');
    }
    function books(){
        $this->subhead = "manage books";
        $crud = new grocery_CRUD();
        $crud->set_table('books');
        $output = $crud->render();
        $this->_admin_output($output);
    }
    function readers(){
        $this->subhead = "manage readers";
        $crud = new grocery_CRUD();
        $crud->set_table('reader');
        $crud->unset_fields('password');
            $crud->callback_before_insert(array($this,'encrypt_password_callback'));
        $output = $crud->render();
        $this->_admin_output($output);
    }
function encrypt_password_callback($post_array) {
  $post_array['password'] = md5($post_array['password']);
  return $post_array;
}   
function borrows(){
        $this->subhead = "manage borrows";
        $crud = new grocery_CRUD();
        $crud->set_relation('book_id','books','name');
        $crud->set_relation('reader_id','reader','name');
        $crud->set_table('borrows');
        $output = $crud->render();
        $this->_admin_output($output);
    }
   
    public function _admin_output($output = null) {

        $this->load->view('view.php', $output);
    }
}
?>
