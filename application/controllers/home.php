<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Website_Controller {

    function __construct() {
       parent::__construct();
       $this->head = "Books Library";
       $this->subhead = "list books";
       $this->load->model('books_model');
       $this->is_admin = $this->session->userdata('is_admin');
       if($this->username = $this->session->userdata('username'))
       $this->userdata = $this->db->get_where('reader',array('name'=>$this->username))->row();
    }

	public function index()
	{
		if(!$this->input->post('signin'))
			$this->books_model->login();
		if($this->username)
			redirect('home/listbook');
		$this->load->view('login');
	}
	public function listbook()
	{
		if(!$this->username)
			redirect('home');
		$this->data['books'] = $this->books_model->listbooks();
		$this->load->view('books',$this->data);
	}
	public function addborrow($id=null)
	{
		if(!$this->username)
			redirect('home');
		if($id){
			$add['book_id'] = intval($id);
			$add['reader_id'] = $this->userdata->id;
			$this->books_model->addborrow($add);
			}
		redirect('home/listborrow');
	}
	public function listborrow()
	{
		if(!$this->username)
			redirect('home');
		$this->subhead = "list borrows";
		$this->data['list'] = $this->books_model->listborrow();
		$this->load->view('borrow',$this->data);
	}
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('home');

	}
	
	
}
